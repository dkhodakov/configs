# Configuration files

All config files written by myself or taken from other authors.

This repo contains configs for:
- **Awesome wm**
- **Vim**
- **Zsh**
- **Bash**
- **Git**
- **Conky**


To install all configs on you system execute:

**`$ ./config_deploy.sh`**

To commit all configs this repo execute:

**`$ ./config_upload.sh`**
